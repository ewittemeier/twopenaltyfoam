#!/usr/local_rwth/bin/zsh
#SBATCH --time=0-20:00:00
# ask for eight tasks
#SBATCH --ntasks=16
# ask for 1 GB memory per task
#SBATCH --mem-per-cpu=1024M   #M is the default and can therefore be omitted, but could also be K(ilo)|G(iga)|T(era)
# name the job
#SBATCH --job-name=touching2
# declare the merged STDOUT/STDERR file
#SBATCH --output=output.%J.txt


### load the necessary module files
module switch intel gcc
module load TECHNICS
module load openfoam
decomposePar
  
### start the OpenFOAM binary in parallel, cf.
### http://www.openfoam.org/docs/user/running-applications-parallel.php
$MPIEXEC $FLAGS_MPI_BATCH   twoPenaltyFoam -parallel

reconstructPar


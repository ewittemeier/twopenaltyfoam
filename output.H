// Free energy

dimensionedScalar outIntFE = 0;
volScalarField outLapC = fvc::laplacian(C1);

forAll(C1,celli)
{
    outIntFE += pow( outLapC[celli] - (1/(pow(epsilon.value(),2)))*( pow(C1[celli],2) - 1 )*( C1[celli] - pow(2,0.5)*C0*epsilon ),2) 			    *C1.mesh().V()[celli];
}

scalar outFE = ( (gamma.value()*epsilon.value()) / 2 )*outIntFE.value();
reduce(outFE, sumOp<scalar>());
	
	
// Adhesion Energy
dimensionedScalar outIntAdhesion = 0;

forAll(C1,celli)
{
    outIntAdhesion += (1/epsilon.value())*C1[celli]*(pow(C2[celli],2)-1)*C1.mesh().V()[celli];
}

scalar outAdhesion = sigma.value() * outIntAdhesion.value();
reduce(outAdhesion, sumOp<scalar>());



// Volume
        
dimensionedScalar outTotVol = 0;
dimensionedScalar outIntC = 0;


forAll(C1,celli)
{
    outTotVol += mesh.V()[celli];
    outIntC += C1[celli]*C1.mesh().V()[celli];
}

scalar outVesicleVol = 0.5*( outTotVol.value() + outIntC.value() );
reduce(outVesicleVol, sumOp<scalar>());

        
// Surface area

dimensionedScalar outIntB = 0;

volScalarField outSqrGradC = magSqr(fvc::grad(C1));

forAll(C1,celli)
{
    outIntB += ( (epsilon.value()/2)*outSqrGradC[celli] + (1/(4*epsilon.value()))*pow( (pow(C1[celli],2)-1) ,2)  )*C1.mesh().V()[celli];
}

scalar outB = ( (2*pow(2,0.5))/3 )*(outIntB.value());
reduce(outB, sumOp<scalar>());


// Lagrange multipliers

volScalarField outf = epsilon*fvc::laplacian(C1) - (1/epsilon)*(pow(C1,2)-1)*(C1 + pow(2,0.5)*C0*epsilon);

volScalarField outg = fvc::laplacian(f) - (1/pow(epsilon,2))*(3*pow(C1,2) + 2*pow(2,0.5)*C0*epsilon*C1 - 1)*f;


// Lagrange multipliers scheme

outTotVol = 0;
scalar outIntg = 0;
scalar outIntf = 0;
scalar outIntfg = 0;
scalar outIntff = 0;

        forAll(C1,celli)
        {
            outIntg += outg[celli]*C1.mesh().V()[celli];
            outIntf += outf[celli]*C1.mesh().V()[celli];
            outIntfg += outf[celli]*outg[celli]*C1.mesh().V()[celli];
            outIntff += pow(outf[celli],2)*C1.mesh().V()[celli];
            outTotVol += mag(mesh.V()[celli]);
        }

        
        

scalar outLamda1 = (gamma*outIntg*outIntff - outIntf*gamma*outIntfg).value()/determinant; //The first solution of lamda1
scalar outLamda2 = (abs(outTotVol.value())*gamma*outIntfg - gamma*outIntg*outIntf).value()/determinant; //The second solution of lamda2

reduce(outLamda1, sumOp<scalar>());
reduce(outLamda2, sumOp<scalar>());


        //output
        

        output << outFE;
        output << ", ";
	output << outAdhesion;
        output << ", ";
        output << outVesicleVol;
        output << ", ";
        output << outB;
        output << ", ";
        output << outLamda1;
        output << ", ";
        output << outLamda2;
	output << ", ";
        output << runTime.value();
        output << "\n";
        output.flush();
